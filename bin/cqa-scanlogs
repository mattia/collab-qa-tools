#! /usr/bin/ruby
# Parse log files and extract info about packages that failed to build
# because of dependancies problems.

require 'collab-qa'
require 'optparse'

verbose = false
todofile = nil
disptime = false
restrict = nil
progname = File::basename($PROGRAM_NAME)
id = nil
only_failed = false
pkglist = []

opts = OptionParser::new do |opts|
  opts.program_name = progname
  opts.banner = "Usage: #{progname} [options]"
  opts.separator ""
  opts.separator "Options:"

  opts.on("-v", "--verbose", "Verbose mode") do |v|
    verbose = true
  end

  opts.on("-T", "--with-time", "Display time in output") do |v|
    disptime = true
  end

  opts.on("-t", "--TODO FILE", "Only TODO lines from file") do |f|
    todofile = f
  end

  opts.on("-r", "--restrict RE", "Only lines matching RE") do |r|
    restrict = /#{r}/
  end

  opts.on("-i", "--id TEXT", "Only logs with identifier") do |i|
    id = i
  end
  
  opts.on("-f", "--failed", "Only failed logs") do |f|
    only_failed = true
  end

  opts.on("-o", "--only FILE", "Only packages listed in FILE") do |f|
    pkglist = IO::readlines(f).map { |l| l.chomp }
  end

end
opts.parse!(ARGV)

if todofile
  pkgs = IO::read(todofile).split(/\n/).grep(/ TODO/).map { |e| e.split(' ')[0] }
  files = []
  pkgs.each do |pkg|
    next if pkglist != [] and not pkglist.include?(pkg)
    g = Dir::glob("#{pkg}_*log")
    g2 = Dir::glob("#{pkg}.*log")
    files << g[0] if g[0] != nil
    files << g2[0] if g2[0] != nil
  end
else
  if id
    files = Dir::glob("*_#{id}.log")
  else
    files = Dir::glob("*log")
  end
end

if files.empty?
  STDERR.puts "No (matching) log files found in current directory."
end

files.sort.each do |file|
  puts "Parsing #{file}" if verbose
  begin
    log = CollabQA::Log::new(file)
    log.extract_log

    next if only_failed and log.result == "OK"

    if restrict
      l = log.oneline_to_s(disptime)
      puts "#{l}" if l =~ restrict
    else
      puts "#{log.oneline_to_s(disptime)}"
    end
  rescue RuntimeError => e
    STDERR.puts "Exception caught while parsing #{file}"
    STDERR.puts e.backtrace
  end
end
