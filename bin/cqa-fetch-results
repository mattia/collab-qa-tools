#!/usr/bin/ruby
# script to fetch the results from ec2, publish them, and prepare the
# local results list
#
# needs:
# export DATE=2007/08/06
#

require 'optparse'
require 'net/ssh'
require 'net/scp'

class CommandException < RuntimeError
  attr :command
  def initialize(command)
    @command = command
  end
end

def ensure_exec_ok(ssh, command)
  ssh.exec! command do |ch, stream, data|
    if stream == :stderr
      raise CommandException.new(command), data
    end
  end
end

def check_env(var, eg)
  if ENV[var].nil?
    puts "#{var} environment variable not set. e.g export #{eg}"
    exit(1)
  end
end

def get_env(var, value)
  if ENV[var].nil?
    return value
  else
    return ENV[var]
  end
end

# check needed ones
check_env('DATE', 'DATE=2011/08/23')
check_env('USER', 'USER=user')

# add some defaults if not defined
ARCH=get_env('ARCH', 'amd64')
CHROOT=get_env('CHROOT', 'unstable')

PERSON=ENV['USER']
DATE=ENV['DATE']

DDATE=DATE.gsub('/', '-')
ID="#{CHROOT}-#{ARCH}.#{DDATE}"
TARGETDIR="#{DDATE}-#{CHROOT}-#{ARCH}"

$logs_dir = "/tmp/logs"


options = OptionParser::new do |opts|
  opts.banner = "Usage: ./fetch-and-process-results-aws [options]"
  opts.separator ""
  opts.separator "Options:"
  opts.on("-d", "--logs-dir LOGSDIR", "Find build logs in rebuildmaster LOGSDIR, defaults to '/tmp/logs'") do |d|
    $logs_dir = d
  end
end
options.parse!(ARGV)

system "mkdir -p #{TARGETDIR}"
Dir::chdir(TARGETDIR)

begin
  Net::SSH.start('rebuildmaster', PERSON) do |ssh|
    
    puts "parsing logs..."
    ssh.exec!("cd #{$logs_dir} && cqa-scanlogs > /tmp/res.#{ID}")

    puts "building log list..."
    ensure_exec_ok(ssh, "cat /tmp/res.#{ID} | grep -v ' OK ' | sed 's/ [0-9]*:/ /' | awk ' { print $1 \"_\" $2 \"_#{CHROOT}.log\" } ' > /tmp/loglist.#{ID}")

    puts "publishing logs..."
    ensure_exec_ok(ssh, "mkdir -p /var/www/ftbfs-logs/#{DATE}")
    ensure_exec_ok(ssh, "cd #{$logs_dir} && rsync -avzP --files-from=/tmp/loglist.#{ID} . /var/www/ftbfs-logs/#{DATE}")

# disabled: we need to find a way to store logfiles compressed
#    ensure_exec_ok(ssh, "~/cloud-scripts/gzip-logs")
    ensure_exec_ok(ssh, "chmod -R a+rX /var/www/ftbfs-logs/#{DATE}")

    puts "copying results file locally..."
    ensure_exec_ok(ssh, "cat /tmp/res.#{ID} | sort > /tmp/res.#{ID}.sorted")
    ssh.scp.download!("/tmp/res.#{ID}.sorted", "fullresults.#{DDATE}.txt")
    ensure_exec_ok(ssh, "rm /tmp/res.#{ID}.sorted")
  end
  
rescue CommandException => e
  puts "error: command \"#{e.command}\" failed, reason: #{e}"
  exit(2)
end

puts "generating list of failed packages..."
system "grep -v ' OK ' fullresults.#{DDATE}.txt > failed.#{DDATE}.tmp"
puts ""
puts "now, merge old results in new file:"
puts "  cd #{TARGETDIR}"
puts "  cqa-merge-results OLDRESULTFILE failed.#{DDATE}.tmp > failed.#{DDATE}.txt"
puts ""
puts "  mkdir /tmp/cqa.#{ARCH}.#{DDATE}"
puts "  rsync -avzP rebuildmaster:/var/www/ftbfs-logs/#{DATE}/* /tmp/cqa.#{ARCH}.#{DDATE}/"

File::open("README", "w") do |f|
  f.puts "Merging old results in new file:"
  f.puts "  cqa-merge-results OLDRESULTFILE failed.#{DDATE}.tmp > failed.#{DDATE}.txt"
  f.puts "  e.g cqa-merge-results ../2011-07-18-lsid64-amd64/failed.txt failed.#{DDATE}.tmp > failed.#{DDATE}.txt"
  f.puts
  f.puts "Retrieving all logs:"
  f.puts "  mkdir /tmp/cqa.#{ARCH}.#{DDATE}"
  f.puts "  rsync -avzP rebuildmaster:/var/www/ftbfs-logs/#{DATE}/* /tmp/cqa.#{ARCH}.#{DDATE}/"
end

Dir::chdir('..')
